% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% Start psychtoolbox
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256; % rgb values [256 256 256] is white, [0 0 0] black
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);

% As in the previous example, we will use a while loop to repeatedly check
% where the mouse is and what it�s doing. However, this time we will collect
% the mouse cursor coordinates in a matrix so we have a record of where the
% mouse was moved before the user clicks on anything.

% Stuff to set up before starting the while loop

dt = 1/200; % This sets a �delay time� of 1/20 second. We will put this into
% the while loop to slow it down so it doesn�t collect an absurd number of
% mouse coordinates in a few seconds.

timeOut = inf; % As in the previous example, the amount of time to wait
% before ending the while loop, again we set it to infinity so the loop will
% run until the user presses a button.

Pressed = 0;
% When Pressed is True the while loop will end. We want to start with it
% False so the loop runs.

count = 1; % We don�t want to collect so much data that it crashes the program,
% so we set a limit as to how many times we will run this loop. We will add
% to the variable count each time through the loop to keep track of how many
% times it runs.

max_count = 20*1000; % When count reaches max_count, the while loop will end.

% A good technique to use with matlab is �pre-allocation�. In some programs
% like python, you might add data to a set by sticking each element onto the
% end. In matlab, building up a matrix bit by bit like that uses up a lot of
% resources. It is better to anticipate how many elements of data you plan
% to collect, then create a matrix of the appropriate size. Then, change the
% elements of the matrix as you collect data rather than making a whole new
% matrix each time.

% Since we plan to collect no more than max_count pieces of data, we can make
% two 1 by max_count matrices to hold our data.

X = nan(max_count,1); % creates a 1 by max_count matrix to hold the x coordinates
Y = nan(max_count,1); % creates a 1 by max_count matrix to hold the y coordinates.

% nan is �not a number�. Before we add our data, all the elements in the matrices will be �nan�s

% Start the while loop
while ~Pressed && (GetSecs < timeOut) && (count < max_count)
    % This while loop has three conditions that must all be True in order for the loop to continue:
    %   1) Pressed is False
    %   2) Computer time obtained with GetSecs is less than timeOut
    %   3) Count has not reached max_count
    
    [x,y,buttons] = GetMouse(window);
    % The function GetMouse will provide an x coordinate, a y coordinate,
    % and a button vector (described in previous)

    Pressed = sum(buttons)>0;
    % Sets Pressed to True if a button has been pressed (this will end the while loop)
    
    X(count) = x; % Count is used to identify an element in the X matrix.
    % This element will be changed to the x coordinate obtained with GetMouse
    
    Y(count) = y; % Count is also used to identify an element in the Y matrix,
    % and this element will likewise be changed to the y coordinate obtained
    % with GetMouse
    
    % By using count, we ensure that the x coordinates in the X matrix occupy
    % the same element as the corresponding y coordinates.

    count = count + 1; % Add one to count on each iteration through the loop.
    % This ensures the x and y coordinates are saved in the correct elements
    % of their respective matrices, and if it exceeds max_count, it will end
    % the while loop.
    
    WaitSecs(dt); % This makes the loop wait for 1/20 seconds before running
    % again. This prevents it from running as fast as possible and collecting
    % more coordinates that we want or need.

end % End the while loop
sca

% Once this code is run, we can plot the X and Y matrices (saved in the workspace)
% to produce a graph of the mouse movement.
plot(X,Y)
xlim([0, A])
ylim([0, B])
