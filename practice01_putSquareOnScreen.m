% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

clear % Gets rid of extra stuff that might be in the command window
input(' ');  % sets cursor into the command window so keypresses don�t get into the program file.
whichScreen = 0; % Could set to display on a second screen if one is available,
% this sets it on the one screen we have
bgColour = [50 50 50]; % rgb values on a scale of 0 to 256. This is grey.

% [256 256 256] is white, [0 0 0] is black.

[window, rect] = Screen('OpenWindow', whichScreen, bgColour); %This opens the window
% �window� is now the name of this window so it is used to refer to it.
% This line also creates variable �rect�. rect is a vector.
% rect = [0 0 1920 1080](size of screen in pixels)
[A,B] = Screen('WindowSize', window);

Screen('FillRect', window, [250 0 0], [100 100 200 200]);
% What the Screen function does depends on the first argument. Here it is
% drawing a filled  rectangle. The second argument puts the rectangle in
% previously named window, the third argument specifies the color (here red)
% and the fourth argument gives the x,y coordinate of the start point and
% the x,y coordinate of the end point as a vector.

% [x coordinate in pixels of upper left corner, y coordinate in pixels of upper left corner, ...
%   x coordinate in pixels of lower right corner, y coordinate in pixels of lower right corner]

% Screen creates the shape but doesn�t display it. In this section, you want
% to lay out everything %that will appear on one screen. So if you want two
% shapes, you would specify the second one now. Then use flip to display all
% the objects at once.

% Here�s a second rectangle to display with the first.
Screen('FillRect', window, [250 250 0], [300 300 500 500]);

flipTime = Screen('Flip', window, [], 1);
% Flip puts the previously defined object on screen. It returns the exact time
% the object appears on screen. The second argument specifies which window.
% The third argument allows a specific time to be specified. 
% Can use GetSecs to get an exact computer time. For example, adding an
% assignment �timeOn = GetSecs + 2�. Putting �timeOn� as the third argument
% means the figure will display after 2 seconds. The fourth argument is �dontclear�.
% 0 in this argument means the objects will be cleared when a new screen comes up,
% 1 means they will remain.

% Display a second set of rectangles after a specified time
Screen('FillRect', window, [250 0 250], [200 150 500 200]); % Create more rectangles
Screen('FillRect', window, [250 250 250], [300 300 500 500]);

% display the new objects. If 1 was selected in the last use of flip, the
% previous objects will stay, if 0, they will go away
flipTime = Screen('Flip', window, timeOn2, 0);

WaitSecs(2) % Tells psychtoolbox to wait two seconds (can put this anywhere
% in the code you want it to pause)
sca % Exits psychtoolbox
