% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% This is the functionalized version of practice03_waitForInput, reference
% that script for detailed explanations of what some of the lines do.

% A version that has the correct file name should be saved in the
% repository as waitForKeyPress.

function [KeyNum, when] = waitForKeyPress(keyName, waitPeriod)
% The function will take a valid key and a length of time to wait before
% timing out. It will return the number associated with the key pressed and
% the time it was pressed.

for i = 1:length(keyName)
    keys(i) = KbName(keyName{i});
end

% time out at infinity for now
timeOut = GetSecs+waitPeriod;

% wait for a keypress for TimeOut seconds
Pressed = 0;

while ~Pressed && (GetSecs < timeOut)
    [key, when, keycode] = KbCheck(-1);
    % a valid key was pressed (we ignore invalid keys presses)
    if (key && sum(keycode(keys)))
        
        KeyNum = find(keycode(keys),1);
        Pressed = 1;    
    end
end

if Pressed == 0  
    KeyNum = [];
    when = []; 
end
