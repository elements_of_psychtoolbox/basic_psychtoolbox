% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% Same opening sequence as before
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256;
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);

oldTextSize = Screen('TextSize', window, 80);
DrawFormattedText(window, 'Hello World', 100, 100, [255 0 0], 70);
flipTime = Screen('Flip', window, [], 0);
% Up to here is the same as practice02_putTextOnScreen

% Define a set of �valid� keys. This way psychtoolbox can ignore that subject
% who just hits keys at random (some people just want to see the world
% burn).
keyName = {'a' 's' 'p'}; % This is a cell array of strings (way to put things
% like strings in a vector-type structure because matlab loves it some vectors)

for i = 1:length(keyName)
    keys(i) = KbName(keyName{i});
end
% This for loop iterates over the specified keys and assigns them a number
% which will be used to refer to them for the rest of the program. Here, since
% there are three letters, we get 1, 2, and 3.

validKeys = [1 2]; %This limits the valid keys to 1 and 2. 3, which corresponds
% to �p� is not going to be used in this section of the program. Another part
% of the program might define [3] as the only valid key, so that�s why we wanted
% to include p in the first place.

timeOut = inf; % Sets time out to infinity (the screen will sit and wait for
% input for as long as it takes without timing out). If a number is put in
% place of �inf�, the program will wait for a keypress for that many seconds.

Pressed = 0; % Since no key has been pressed, we create the variable Pressed
% and set it to False (0). This way ~Pressed is True in the next line.

while ~Pressed && (GetSecs < timeOut) % Set up a while loop
    [key, when, keycode] = KbCheck(-1);
    % KbCheck is a function that returns which key, when it was pressed,
    % and the keycode
    % key is either 0 or 1, 0 if an invalid key was pressed and 1 if it was a
    % valid key (defined above with the validKeys line)
    if (key && sum(keycode(keys)))
        % This statement will be false and this part of the code won�t execute
        % if key = 0 (key is False) or sum(keycode(keys))) = 0 (is False).
        % If the key was a, which was set to 1 in the for loop above, sum(keycode(keys))) = 1.
        % If the key was s, sum(keycode(keys))) = 2.
        
        KeyNum = find(keycode(keys),1);
        if ~isempty(intersect(KeyNum, validKeys))
            % More commands that ensure this part of the code only executes
            % if a valid key was pressed.
            Pressed = 1; % If all the requirements are met for a valid key,
            % Pressed is set to 1 (True) so ~Pressed is False and the while loop ends.
            % Otherwise it just keeps looping and checking for a key press.
        end
    end
end


if Pressed == 0 % This is here for cases when timeOut doesn�t equal infinity
    % and the while loop times out without anything being pressed. Normally
    % the key that was pressed and the time would be saved, here, they are empty sets.    
    KeyNum = [];
    when = [];   
end

% Once a key is pressed and the while loop ends, this text will be created and displayed.
DrawFormattedText(window, 'Bye-bye World', 100, 100, [255 0 0], 70);
flipTime = Screen('Flip', window, [], 0);

WaitSecs(2) %Waits two seconds before closing (Lets us actually see the text being displayed)
sca %Closes the program
