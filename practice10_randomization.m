% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% This script deals with randomization in Matlab, which is a critical
% background piece fo most Psycthoolbox experiments.

% CRITICAL
% Every time matlab starts, it resets to the same random seed. This is useful
% for many things but NOT for experiments. It means if you restart matlab
% every time you run the experiment, you will be showing everything in the
% same order to every subject.

rSeed = GetSecs;
% Since computers can�t be truly random, a good way to start is to get the
% exact computer time obtained with GetSecs. Subjects will not be launching
% the experiment at the exact same second.

% Note that �now� which we used previously to get the date and time for the
% filename, does not change as quickly as GetSecs. Using now could result
% in multiple subjects starting with the same random seed.

% It might not be a bad idea to save rSeed when you save your data. This way,
% if there is an issue with a set of data, you can start the experiment on
% the same random seed and see exactly what stimuli the subject saw.

rand('seed',rSeed) % Actually sets the random seed to the value in the
% variable rSeed (which we set to the exact computer time)

% Generating some random numbers:

x = rand; % rand will return a random number between 0 and 1.

% rand(A,B) will return an A by B matrix of random numbers.
x = rand(10,2);

% permute a bunch of numbers
randperm(10)
% What this will do is return the numbers 1 to 10, but in a random order.
% This order can be taken to refer to the elements of a vector of length
% 10, and then you have a template for "shuffling" the items in the vector.



