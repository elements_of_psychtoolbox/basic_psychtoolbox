% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% This scipt uses two images, Bob.jpg and Bob2.jpg. In this example, these
% were saved in the dropbox but you can use any folder that you know the
% path to.

% This script is practice05_displayImages, but modified to display two
% rather than one image.

% Usual start up code
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256; % rgb values [256 256 256] is white, [0 0 0] black
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);

% All images you intend to use during the program should be uploaded and
% converted into textures at the beginning of the program, even if they won�t
% actually be flipped until much later. Here we upload 2 images to use in
% this script.

cd ~/Dropbox/NeuRLLab_behavioral/EXAMPLE_Psychtoolbox/functions % Tell matlab where to look
BOB_IMAGE = imread('Bob.jpg');
BOB_IMAGE2 = imread('Bob2.jpg');
% Read from file and save using imread function

% resize image
scaleFactor = 0.5;
I_resized = imresize(BOB_IMAGE, [100 100]);
I_resized2 = imresize(BOB_IMAGE2, [100 100]);

% Convert the images into textures. Again, like uploading, this should happen
% before running any other parts of the program.
texture(1) = Screen(window, 'MakeTexture', I_resized);
texture(2) = Screen(window, 'MakeTexture', I_resized2);
% texture(1) and texture(2) are pointers. The textures are stored in a
% texture vector, if you enter �texture� in the command window you will get
% back [11 12]. This is a good way to store large objects like images
% because you can pass around the pointer and not a large image matrix.

% Set size and position of the images
pos = [100 200 1000 500];
pos2 = [100 200 1000 500];

for i = 1:10 % Sets up a loop of jumping back and forth between the images.
    %(not a vital part of this program, it is just for fun).
    
    % Same use of screen and flip time as previous example
    Screen('DrawTexture', window, texture(1), [], pos); 
    flipTime = Screen('Flip', window, [], 0);
    WaitSecs(0.5)
    
    Screen('DrawTexture', window, texture(2), [], pos2);
    flipTime = Screen('Flip', window, [], 0);
    WaitSecs(0.5)
    
end % Ends the loop

sca
