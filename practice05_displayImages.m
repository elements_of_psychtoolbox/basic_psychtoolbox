% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% This scipt uses two images, Bob.jpg and Bob2.jpg. In this example, these
% were saved in the dropbox but you can use any folder that you know the
% path to.

% Usual set of commands to open psychtoolbox and set up the window, etc.
% See previous examples for details about these
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256;
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);

% Need to change directory so matlab knows where to look for the image
cd ~/Dropbox/NeuRLLab_behavioral/EXAMPLE_Psychtoolbox/functions
% You can replace this with your own choice of location

% imread is the function that reads the image from the specified file and
% saves it as a matlab array. We saved this as BOB_IMAGE.
BOB_IMAGE = imread('Bob.jpg');

% If needed, we can resize the image so it will fit correctly on the screen.
scaleFactor = 0.5;
% If we wanted to scale by a factor, we need this line to specify the factor.

I_resized = imresize(BOB_IMAGE, [100 100]);
% imresize is the function that resizes the image. The last argument here [100 100]
% tells it to shrink the image to 100 by 100 pixels. If we had wanted to shrink
% it by half, we would have put scaleFactor in as the second argument
% instead of [100 100].

% Psychtoolbox doesn�t display images, it displays textures. So we need to
% turn our image into a texture.
texture = Screen(window, 'MakeTexture', I_resized);
% Note that this is our friend the screen function from earlier, it does a
% lot of different things depending on the arguments given. Here it makes
% the image into a texture. The first argument specifies the window (here we
% have only one), the second argument tells it to make a texture, and the
% third argument specifies which image to make into a texture.

% Display the texture
pos = [100 200 1000 500]; % This line specifies the coordinates (in pixels)
% for the texture. If the texture is not this size, it will be stretched to fit.
% 100 200 is the x,y coordinates of the upper left corner and 1000 500 is the
% x,y coordinates of the lower right corner of the texture.

Screen('DrawTexture', window, texture, [], pos);
% Here screen sets up the texture prior to it being flipped. The first argument
% tells screen that it will be displaying a texture, the second specifies the
% window, the third argument tells screen which texture to display (here we
% have only the one created two lines previously and creatively named �texture�).
% The final argument, pos, passes the pixel coordinates we previously set up
% so screen knows what size and where to draw the texture.

flipTime = Screen('Flip', window, [], 0);
% Flip puts the texture we just made on screen. It returns the exact time it
% appeared on screen.
% The second argument specifies which window.
% The third argument allows a specific time to be specified. As noted previously,
%   we could have used GetSecs to get an exact computer time in a previous line
%   and then passed �GetSecs + 2� to have the texture display after 2 seconds.
% The fourth argument is �dontclear�. 0 in this argument means the objects will
% be cleared when a new screen comes up, 1 means they will remain.

WaitSecs(2) % Wait two seconds
sca % End the program 
