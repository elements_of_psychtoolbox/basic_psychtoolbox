% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% Open up psychtoolbox in the usual way
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256; % rgb values [256 256 256] is white, [0 0 0] black
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);

% To wait for input from the mouse, we use a while loop. Note similarities
% to waiting for keyboard input. Every iteration through the loop reports
% where the mouse is and what it's doing.

timeOut = inf; % We could set a length of time to time out, here we set it
% to infinity so it will never time out.

Pressed = 0;
% When Pressed is True the while loop will end. We want to start with it
% False so the loop runs.

% Start the loop
while ~Pressed && (GetSecs < timeOut)
    % Start a while loop that will run until there is mouse input (Pressed is True)
    % or the time runs out (which it won�t, because we just set timeOut to infinity).

    [x,y,buttons] = GetMouse(window); 
    % The function GetMouse returns the x,y coordinates of the cursor plus
    % a three element vector representing the mouse buttons. [0 0 0] means
    % no buttons are being pressed, while [1 0 0] would mean the left button
    % is pressed, and so on.

    Pressed = sum(buttons)>0;
    % If sum(buttons) is greater than 0, it means one or more of the elements
    % in the button vector from GetMouse is 1, meaning a button was pressed.
    % If a button was pressed, then the statement sum(buttons)>0 is True, so
    % Pressed is set to True and this will terminate the while loop.
end
sca
