% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% Start psychtoolbox in the usual way.
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256; % rgb values [256 256 256] is white, [0 0 0] black
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);


% Set everything up for a while loop to continuosly collect mouse information
% (this is described in detail in practice08_mouseInput_part2)
dt = 1/200; % Set a wait time
timeOut = inf; % Sets time out to infinity
max_count = 20*1000; % Sets limit on the amount of data to collect

for trial_count = 1:10 % We are going to collect 10 responses (clicks)
    
    DrawFormattedText(window, num2str(trial_count), 100, 100, [255 0 0], 70);
    % This will display the response number we are on (helps make sure this
    % practice code is working)
    flipTime = Screen('Flip', window, [], 0); % Display the number
    
    X = nan(max_count,1); % Make an empty matrix to hold the x coordinates of the mouse
    Y = nan(max_count,1); % Make another empty matrix to hold the y coordinates
    
    count = 1; % Start the count
    Pressed = 0; % Set pressed to False
    while ~Pressed && (GetSecs < timeOut) && (count < max_count) % Start the while loop
        
        [x,y,buttons] = GetMouse(window); % Check the mouse each time through the while loop.
        Pressed = sum(buttons)>0; % Change Pressed to True when the mouse is clicked.
        
        if Pressed
            when = GetSecs; % Get the computer time when the mouse is pressed
            % and save it as the variable when
        end % End the while loop
        
        X(count) = x;
        Y(count) = y;
        count = count + 1;
        
        WaitSecs(dt); % Need to pause it to keep it from running through the
        % loop too quickly and crashing
    end
    % After the while loop ends, save the final mouse position
    % (the one it was at when it was clicked) as X and Y.
    data(trial_count).X = X;
    data(trial_count).Y = Y;
    data(trial_count).time = when; % Remember we saved the computer time when the mouse was pressed
    % data is a struct with the fields X, Y and time. In each field, there
    % are as many entries as times we ran through the loop.
    
    % Simply creating a variable name dot something and assigning a value
    % tells matlab to make a struct.
    
    % my_awesome_struct.cool_variable = variable
    
    % Can enter plot(data(1).X, data(1).Y) to plot all the mouse movements
    % that were made while the program was waiting for input.
    
    WaitSecs(0.2);
     
end
sca % sca is up here because you don�t need psychtoolbox open to save data, and that is the next step.

subjectID = '001';
% Set a subject number. This is hard coded into this code, but in many experiments
% we ask the experimenter to enter it each time.

dateAndTime = datestr(now);
% The function datestr with argument now returns the computer date and time
% as a string. Here we save the string as the variable dateAndTime.

% The string contains spaces, colons, and hyphens, which make for an untidy
% filename. The next three lines are sting commands to remove them.
dateAndTime(dateAndTime==' ') = [];
dateAndTime(dateAndTime==':') = [];
dateAndTime(dateAndTime=='-') = [];

filename = ['expt_' subjectID '_' dateAndTime];
% Create a filename by concatenating the subject id and dateAndTime.

% Set a directory where you would like to save the data set
datadir = '~/Desktop/'; % Here it is saving to the desktop

save([datadir filename]) % This saves every variable that is currently in
% workspace, 

% If we want to be more specific in our saving, we can add strings to the
% save function with the names of the variables we want to save
save([datadir filename], 'data', 'dt')

