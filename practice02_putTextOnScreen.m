% This is adapted from notes I took on some basic demos Bob gave in
% Fall 2015. It inclues the code itself, and a lot of comments I made in
% the course of explaining it to myself. Vera 12/7/16.

% Same opening sequence as before
clear
input(' ');
whichScreen = 0;
bgColour = [0 0 0]+256;
[window, rect] = Screen('OpenWindow', whichScreen, bgColour);
[A,B] = Screen('WindowSize', window);
% Up to here is the same as practice01_putSquareOnScreen

oldTextSize = Screen('TextSize', window, 80); % Argument 3 sets the text size

Screen('FillRect', window, [250 250 0], [100 100 500 500]);
% Create a rectangle to go behind the text. Objects display in the order
% they are created from front to back. So if you don�t want your text covered
% by other stuff, it should be the last thing created.

DrawFormattedText(window, 'Hello World', 100, 100, [255 0 0], 70);
% Creates the text
% The first argument "window" specifies the window it displays in (here there is only one)
% Second argument supplies the string to display
% Third argument is x coordinate in pixels of upper left corner
% Fourth argument is y coordinate in pixels of upper left corner
% Fifth argument is a red/green/blue vector (here the text will be red)
% Sixth argument is �WrapAt� which tells how many characters to wrap after.

flipTime = Screen('Flip', window, [], 0);
% As in previous example, once everything has been created flip displays it.

WaitSecs(2) %Waits two seconds before closing
sca %Closes the program
